<?php

	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');

	include("bootstrap.php");

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	/* Select queries return a resultset */
	$result = $mysqli->query("SELECT * FROM User_Table WHERE IsActive = 1");


	$i = 0; 
	if($result){
     // Cycle through results
         	while ($row = $result->fetch_array()){        	
    		$user_arr[$i]['value'] = $row["uID"];
    		$user_arr[$i]['label'] = $row["Name"] ;	
    		$i = $i + 1;
    	}
    	// Free result set
    	$result->close();
	}
	$mysqli->close();

	echo json_encode($user_arr);

?>
