<?php
	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');

	$uID = $_POST["Id"];
	$vID = $_POST["Award"];
	$description = $_POST["Description"];

	$imageURL = '';

	$tempURL = '../temp/image64.png';
	//save base 64
		if(!empty($_POST['imagebase64'])){
        	$data = $_POST['imagebase64'];

	        list($type, $data) = explode(';', $data);
    	    list(, $data)      = explode(',', $data);
        	$data = base64_decode($data);

        	file_put_contents($tempURL, $data);

        	$target_dir = "../heroes/";
			$file = "image-".random_string(12);
   	 		$ext =".jpg";

        	$imageURL = $target_dir. $file . $ext;
   			//compress files
  			 compressImage($tempURL,$imageURL,60); 	
    	}

	$date = $_POST["Awarddate"];

	$response = new StdClass();
	$response->result = "";
	$response->msg = "";
	$error = false;
 
	include("bootstrap.php");
// format the date
	$newdate = str_replace('/', '', $date );
	$awarddate = date("Ymd", strtotime($newdate));

/*
// If a hero image was uploaded check the extension, compress it save it to the uploads file with // the award date as an extension
	if (($_FILES['imgfile']['name']!="")){
// Where the file is going to be stored
		$target_dir = "../heroes/";
		$file = $_FILES['imgfile']['name'];
		$path = pathinfo($file);
		$filename = $path['filename'];
		$ext = $path['extension'];
		$temp_name = $_FILES['imgfile']['tmp_name'];
		$path_filename_ext = $target_dir.$filename.".".$ext; 
		$valid_ext = array('png','jpg');

		compressImage($tempURL,$path_filename_ext,60);
	 	$imageURL = str_replace("../heroes/","",$path_filename_ext);

// Check that the extension is valid
  		if(in_array($ext,$valid_ext)){
// Check if file already exists
			if (file_exists($path_filename_ext)) {
				unlink($path_filename_ext);
			}
	 		compressImage($tempURL,$path_filename_ext,60);
	 		$imageURL = str_replace("../heroes/","",$path_filename_ext);
	 	}
	 	else {
 			$response->result = "error";
			$response->msg = "Invalid file type";
 			$myJSON = json_encode($response);
			echo $myJSON;
			$error = true;
 		}
 		
	}
*/
// If there are no errors add a record to the awards table
	if ($error == false) {
		$query = "INSERT INTO Awards_Table (AwardDate, uID, vID, Description, HeroImageURL) VALUES ('".$awarddate."','".$uID."','".$vID."','".$description."','".$imageURL."')";
		if ($mysqli->connect_errno) {
			$response->result = "error";
			$response->msg = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			$myJSON = json_encode($response);
			echo $myJSON;
			$error = true;
		} else {
			if ($mysqli->query($query) === TRUE) {			
				$response->result = "success";
				$response->msg = "Award added successfully - filename: " . $_POST['imagebase64'] ;
				$myJSON = json_encode($response);
				echo $myJSON;
			} else {
				$response->result = "error";
				$response->msg = $query . "<br>" . $mysqli->error;
					$myJSON = json_encode($response);
				echo $myJSON;
				$error = true;
			}	
		$mysqli->close();
		}
	}
// Compress image
function compressImage($source, $destination, $quality) {
  $info = getimagesize($source);

  if ($info['mime'] == 'image/jpeg') 
    $image = imagecreatefromjpeg($source);

  elseif ($info['mime'] == 'image/gif') 
    $image = imagecreatefromgif($source);

  elseif ($info['mime'] == 'image/png') 
    $image = imagecreatefrompng($source);
  imagejpeg($image, $destination, $quality);

}
function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

?>
