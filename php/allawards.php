<?php

    header("Access-Control-Allow-Headers: Authorization, Content-Type");
    header("Access-Control-Allow-Origin: *");
    header('content-type: application/json; charset=utf-8');
    $cur_awards = [];
    $awards_arr = [];
    $response = new StdClass();
    $response->result = "";
    $response->msg = "";
    include("bootstrap.php");

    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    else {
// Select all Values 
        $result = $mysqli->query("SELECT * FROM Value_Table");
        $i = 0; 
        
// Read all the awards in reverse order       
        $query2 = $mysqli->query("SELECT a.*, b.*, c.* FROM Awards_Table a, User_Table b, Value_Table c WHERE a.vID=c.vID AND a.uID=b.uID ORDER BY a.AwardDate DESC");
        if($query2){
            $i = 0; 
// Cycle through results
            while ($row3 = $query2->fetch_array()){ 
// If this isn't a current award
           
                    $awards_arr[$i]['id'] = $row3["aID"];
                    $awards_arr[$i]['value'] = $row3["AwardName"];
// Format the date
                    $date =  $row3["AwardDate"];
                    $awards_arr[$i]['date'] = date("d/m/Y", strtotime($date));
                    $awards_arr[$i]['name'] = $row3["Name"];
                    $awards_arr[$i]['jobtitle'] = $row3["JobTitle"];
                    $awards_arr[$i]['location'] = $row3["Location"];
                    $awards_arr[$i]['description'] = $row3["Description"];
// If a hero image has been uploaded pass the URL, otherwise pass the user image URL            
                    if ($row3["HeroImageURL"]!="") {
                        $awards_arr[$i]['ImageURL'] = $row3["HeroImageURL"];
                    }
                    else if ($row3["ImageURL"]!="") {
                        $awards_arr[$i]['ImageURL'] = "uploads/".$row3["ImageURL"];
                    }
                    else {
                        $awards_arr[$i]['ImageURL'] = "dist/assets/img/avatar.jpg";
                    }
                    $i = $i + 1;
                
            }
            $query2->close();
        }
        $mysqli->close();
        echo json_encode($awards_arr);
    }
?>