<?php

	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');

	$user_arr = [];
	include("bootstrap.php");

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	/* Select queries return a resultset */
//	$result = $mysqli->query("SELECT * FROM User_Table ORDER BY Name");

    $result = $mysqli->query("SELECT * FROM User_Table where IsActive = 1 ORDER BY RAND()");
    
	$i = 0; 

	if($result){
     // Cycle through results
         while ($row = $result->fetch_array()){  	
    		$user_arr[$i]['id'] = $row["uID"];
    		$user_arr[$i]['name'] = $row["Name"];
    		$user_arr[$i]['email'] = $row["Email"];	
    		$user_arr[$i]['jobtitle'] = $row["JobTitle"];	
    		$user_arr[$i]['location'] = $row["Location"];	
    		if ($row["ImageURL"]!="") {
                $user_arr[$i]['ImageURL'] = "uploads/".$row["ImageURL"];
            }
            else {
            	$user_arr[$i]['ImageURL'] ="dist/assets/img/avatar.jpg";
            }
    		$user_arr[$i]['isactive'] = $row["IsActive"];
    		$i = $i + 1;
    	}
    	// Free result set
    	$result->close();
	}
	$mysqli->close();
	 echo json_encode($user_arr);
?>
