<?php
	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');

	$name = $_POST["Name"];
	$email = $_POST["Email"];
	$location = $_POST["Location"];
	$jobtitle = $_POST["Jobtitle"];
	$imageURL = '';

	$tempURL = '../temp/new-image64.png';
	//save base 64
		if(!empty($_POST['imagebase64'])){


		
        	$data = $_POST['imagebase64'];

	        list($type, $data) = explode(';', $data);
    	    list(, $data)      = explode(',', $data);
        	$data = base64_decode($data);

        	file_put_contents($tempURL, $data);

        	$target_dir = "../uploads/";
			$file = "image-".random_string(12);
   	 		$ext =".jpg";

			$imageName = $file . $ext;
        	$imageURL = $target_dir. $file . $ext;
   			//compress files
  			 compressImage($tempURL,$imageURL,60); 	
    
    	}


	$response = new StdClass();
	$response->result = "";
	$response->msg = "";
	$error = false;
// start by checking whether this user already exists


	include("bootstrap.php");
// Check whether the user exists already
	$query = mysqli_query($mysqli, "SELECT * FROM User_Table WHERE Email='".$email."' LIMIT 1");
	if ($mysqli->connect_errno) {
		$response->result = "error";
		$response->msg = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		$myJSON = json_encode($response);
		echo $myJSON;
		$error = true;
	} 
	else {

		if(mysqli_num_rows($query) > 0){
			$response->result = "error";
			$response->msg = "A user already exists for this email address";
			$myJSON = json_encode($response);
			echo $myJSON;
			$error = true;
   		}
   	}
   //	if ($error == false) {
// If an image was uploaded validate it, compress it and save it in the uploads file
	//	if (($_FILES['imgfile']['name']!="")){
// Where the file is going to be stored
	//		$target_dir = "../uploads/";
	//		$file = $_FILES['imgfile']['name'];
	//		$path = pathinfo($file);
	//		$filename = $path['filename'];
	//		$ext = $path['extension'];
	//		$temp_name = $_FILES['imgfile']['tmp_name'];
	//		$path_filename_ext = $target_dir.$filename.".".$ext; 
	//		$valid_ext = array('png','jpg', 'gif');
// Check if file already exists
	//		if (file_exists($path_filename_ext)) {
	//			$response->result = "error";
	//			$response->msg = "Sorry, a file with this filename already exists";
	 //			$myJSON = json_encode($response);
	//			echo $myJSON;
	//			$error = true;
	 //		} 
	//	 	else {
// Check that the extension is valid
	//		  	if(in_array($ext,$valid_ext)){
	//		 		compressImage($_FILES['imgfile']['tmp_name'],$path_filename_ext,60);
	//		 		$imageURL = str_replace("../uploads/","",$path_filename_ext);
	//		 	}
	//		 	else {
	//	 			$response->result = "error";
	//				$response->msg = "Invalid file type";
	//	 			$myJSON = json_encode($response);
	//				echo $myJSON;
	//				$error = true;
	//	 		}
	//	 	}
	//	}
	//}
// If there are no errors add a record to the user table
	if ($error == false) {
		$query = "INSERT INTO User_Table (Name, Email, JobTitle, Location, ImageURL, IsActive) VALUES ('".$name."','".$email."','".$jobtitle."','".$location."','".$imageURL."','1')";
		if ($mysqli->connect_errno) {
			$response->result = "error";
			$response->msg = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			$myJSON = json_encode($response);
			echo $myJSON;
			$error = true;
		} else {
			if ($mysqli->query($query) === TRUE) {			
				$response->result = "success";
				$response->msg = "User added successfully";
				$myJSON = json_encode($response);
				echo $myJSON;
			} else {
				$response->result = "error";
				$response->msg = $query . "<br>" . $mysqli->error;
					$myJSON = json_encode($response);
				echo $myJSON;
				$error = true;
			}	
		$mysqli->close();
		}
	}
// Compress image
function compressImage($source, $destination, $quality) {

  $info = getimagesize($source);

  if ($info['mime'] == 'image/jpeg') 
    $image = imagecreatefromjpeg($source);

  elseif ($info['mime'] == 'image/gif') 
    $image = imagecreatefromgif($source);

  elseif ($info['mime'] == 'image/png') 
    $image = imagecreatefrompng($source);

  imagejpeg($image, $destination, $quality);

}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}


?>
