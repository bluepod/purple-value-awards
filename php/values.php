<?php

	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');
	$value_arr = [];
	include("bootstrap.php");

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	/* Select queries return a resultset */
	$result = $mysqli->query("SELECT * FROM Value_Table");

	$i = 0; 
	if($result){
     // Cycle through results
         	while ($row = $result->fetch_array()){        	
    		$value_arr[$i]['id'] = $row["vID"];
    		$value_arr[$i]['award_name'] = $row["AwardName"];
    		$i = $i + 1;
    	}
    	// Free result set
    	$result->close();
	}
	$mysqli->close();

	echo json_encode($value_arr);

?>