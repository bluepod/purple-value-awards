<?php
	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');

	$uID = $_POST["Id"];
	$name = $_POST["Name"];
	$email = $_POST["Email"];
	$location = $_POST["Location"];
	$jobtitle = $_POST["Jobtitle"];
	$isactive = $_POST["Isactive"];
	//$image = $_POST["userImage"];

	$imageURL = '';
	$tempURL = '../temp/new-image64.png';
	//save base 64
		if(!empty($_POST['imagebase64'])){
        	$data = $_POST['imagebase64'];

	        list($type, $data) = explode(';', $data);
    	    list(, $data)      = explode(',', $data);
        	$data = base64_decode($data);

        	file_put_contents($tempURL, $data);

        	$target_dir = "../uploads/";
			$file = "image-".random_string(12);
   	 		$ext =".jpg";

        	$imageURL = $file . $ext;
   			//compress files
  			 compressImage($tempURL,$target_dir.$imageURL,60); 	
    	} else {
    		//otherwise user existing

    		if ($_POST["userImage"] != 'assets'){

			$imageURL = $_POST["userImage"];

			}
    	}


	//$isactive = $_POST["Isactive"];
	$response = new StdClass();
	$response->result = "";
	$response->msg = "";
	$error = false;
 
	include("bootstrap.php");
// Start by getting the existing details - oif the email has changed make sure the new email 
// doesn't already exist 	
	$query = mysqli_query($mysqli, "SELECT * FROM User_Table WHERE uID='".$uID."' LIMIT 1");
	if ($mysqli->connect_errno) {
		$response->result = "error";
		$response->msg = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		$myJSON = json_encode($response);
		echo $myJSON;
		$error = true;
	} 

// If there are no errors update the record in user table
	if ($error == false) {
		if ($isactive) {
			$actcode = '1';
		}
		else {
			$actcode = '0';
		}
		$query = "UPDATE User_Table SET Name='".$name."', Email='".$email."', JobTitle='".$jobtitle."', Location='".$location."', ImageURL='".$imageURL."', IsActive='".$actcode."' WHERE uID=$uID";
		if ($mysqli->connect_errno) {
			$response->result = "error";
			$response->msg = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			$myJSON = json_encode($response);
			echo $myJSON;
			$error = true;
		} 
		else {
			if ($mysqli->query($query) === TRUE) {			
				$response->result = "success";
				$response->msg = "User updated successfully";
				$myJSON = json_encode($response);
				echo $myJSON;
			} else {
				$response->result = "error";
				$response->msg = $query . "<br>". $query . $mysqli->error;
					$myJSON = json_encode($response);
				echo $myJSON;
				$error = true;
			}	
		$mysqli->close();
		}
	}
// Compress image
function compressImage($source, $destination, $quality) {

  $info = getimagesize($source);

  if ($info['mime'] == 'image/jpeg') 
    $image = imagecreatefromjpeg($source);

  elseif ($info['mime'] == 'image/gif') 
    $image = imagecreatefromgif($source);

  elseif ($info['mime'] == 'image/png') 
    $image = imagecreatefrompng($source);

  imagejpeg($image, $destination, $quality);

}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}
?>
