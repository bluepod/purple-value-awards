<?php
	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');

	$aID = $_POST["Id"];


	$description = $_POST["Description"];
	$date = $_POST["Awarddate"];
	$imageURL = '';
	$response = new StdClass();
	$response->result = "";
	$response->msg = "";
	$error = false;

 
	include("bootstrap.php");
// format the date
	$newdate = str_replace('/', '', $date );
	$awarddate = date("Ymd", strtotime($newdate));

	$imageURL = '';

	$tempURL = '../temp/image64.png';
	//save base 64
		if(!empty($_POST['imagebase64'])){
        	$data = $_POST['imagebase64'];

	        list($type, $data) = explode(';', $data);
    	    list(, $data)      = explode(',', $data);
        	$data = base64_decode($data);

        	file_put_contents($tempURL, $data);

        	$target_dir = "../heroes/";
			$file = "image-".random_string(12);
   	 		$ext =".jpg";

        	$imageURL = $target_dir. $file . $ext;
   			//compress files
  			 compressImage($tempURL,$imageURL,60); 	
    	}

// If there are no errors update the award
	if ($error == false) {

		$query = 'UPDATE Awards_Table SET '; 
		if(!empty($awarddate)){
			$temp = 'AwardDate="'.$awarddate.'"'; 
			$query = $query . $temp;
		}
		if(!empty($description)){
			$query = $query . ', ';
			$temp = 'Description="'.$description.'"'; 
			$query = $query . $temp;
		}
		if(!empty($imageURL)){
			$query = $query . ', ';
			$temp = 'HeroImageURL="'.$imageURL.'"'; 
			$query = $query . $temp;
		}
		$query = $query .' WHERE aID= ' .$aID .'';

		//var_dump($query);
		//die();
	
		if ($mysqli->connect_errno) {
			$response->result = "error";
			$response->msg = "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			$myJSON = json_encode($response);
			echo $myJSON;
			$error = true;
		} else {
			if ($mysqli->query($query) === TRUE) {			
				$response->result = "success";
				$response->msg = "Award updated successfully";
				$myJSON = json_encode($response);
				echo $myJSON;
			} else {
				$response->result = "error";
				$response->msg = $query . "<br>" . $mysqli->error;
				$myJSON = json_encode($response);
				echo $myJSON;
				$error = true;
			}	
		$mysqli->close();
		}
	}

	
// Compress image
function compressImage($source, $destination, $quality) {
  $info = getimagesize($source);

  if ($info['mime'] == 'image/jpeg') 
    $image = imagecreatefromjpeg($source);

  elseif ($info['mime'] == 'image/gif') 
    $image = imagecreatefromgif($source);

  elseif ($info['mime'] == 'image/png') 
    $image = imagecreatefrompng($source);

  imagejpeg($image, $destination, $quality);
}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}


?>
