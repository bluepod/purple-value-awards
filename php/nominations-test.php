<?php
	header("Access-Control-Allow-Headers: Authorization, Content-Type");
	header("Access-Control-Allow-Origin: *");
	header('content-type: application/json; charset=utf-8');
	$response = new StdClass();
	$response->result = "";
	$response->msg = "";
	$error = false;
	include("bootstrap.php");

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	$name = $_POST["Name"];
	$email = $_POST["Email"];
	$id = $_POST["Id"];
	$jobtitle = $_POST["Jobtitle"];
	$location = $_POST["Location"];
	$nomname = $_POST["inputName"];
	$nomemail = $_POST["inputEmail"];
	$awardPost =  $_POST["Award"] ;
	$description = $_POST["Description"];


if(isset($awardPost)) {
	foreach ($awardPost as $key => $value)
	{
		$award = $value;
	}
}

// Generate an email for the nominations committee

	//$ = "paul,carnell@purple.agency";
	$from = $nomemail;
	$to = 'value.awards@purple.agency';
	
	$subject = 'Nomination for '.$name;

	$headers = "From: " . $from . "\r\n";
	$headers .= "Reply-To: ". $from . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message = $nomname.' (email '.$nomemail.') has nominated '.$name.', '.$jobtitle.' from '.$location.' (email '.$email.') for an award for the following value: ';

	// Get the award desciption
	$result = $mysqli->query("SELECT * FROM Value_Table WHERE vID='".$award."' LIMIT 1");
	if($result->num_rows > 0){
		$value = $result->fetch_assoc();
		$awardText = $value["AwardName"]; 
		$message.=  $awardText;
	}
	$result->close();
	$mysqli->close();

	$message.=" for the following reason: ".$description;

		$response->result = "success";
		$response->msg = "Thank you - your nomination for ".$name."  has been sent to the awards committee";	

/*

	if (mail($to, $subject, $message, $headers)) {
	 	$response->result = "success";
		$response->msg = "Thank you - your nomination for ".$name."  has been sent to the awards committee";


		$nto = $email;
		//$nto = "paul.carnell@purple.agency";
		$nfrom = 'value.awards@purple.agency';
		$nsubject = 'Congratulations - you have been nominated for a Purple value award!';

		$nheaders = "From: " . $nfrom . "\r\n";
		$nheaders .= "Reply-To: ". $nfrom . "\r\n";
		$nheaders .= "MIME-Version: 1.0\r\n";
		$nheaders .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$nmessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
		$nmessage.='<html><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		$nmessage.='<title>Purplicious Nomination</title>';
		$nmessage.='<style>@media only screen and (min-device-width: 601px) {.content {width: 600px !important;}}</style>';
		$nmessage.='</head> <body> ';
		$nmessage.='<!--[if (gte mso 9)|(IE)]><table width="600" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->';
		$nmessage.='<table class="content" align="center" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px;border: 8px solid #2D1433; border-radius: 10px;">';
		$nmessage.='<tr><td style="background: #F3A4BA" align="center"><img src="http://value-awards.purple-agency.net/dist/assets/img/header.jpg" width="584" alt="The Purplicious Awards"></td></tr>';
		$nmessage.='<tr><td style="padding:25px; background-color:#554596; color: #FFF265;font-family: Arial, Helvetica, sans-serif;">';
		$nmessage.='<p>Congratulations</p>';
		$nmessage.='<p>You&#39;re so Purplicious that someone has nominated you for <span style="color: #FFA500;font-family: Arial, Helvetica, sans-serif;">'.$awardText.'</span> for <span style="color: #FFA500;font-family: Arial, Helvetica, sans-serif;">'.$description .'</span>.</p><p>This means you&#39;re in the running for being named the most Purplicious person this month. </p>';
		$nmessage.='<p>It&#39;s great that your colleagues have recognised you for the amazing Purple person you are. Don&#39;t forget to share the love and nominate anyone you think is worthy of sharing the limelight with you.</p>';
		$nmessage.='<p>The Awards Team</p>';
		$nmessage.='</td></tr></table>';
		$nmessage.='<!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->';
		$nmessage.='</body></html>';

		// CAN WWE SEND SECOND EMAIL??
		mail($nto, $nsubject, $nmessage, $nheaders);

	} 
	else {
   		$response->result = "error";
		$response->msg = "Sorry there was a problem with your submission - please try again";
	}
	*/
	
	$myJSON = json_encode($response);
	echo $myJSON;
// Now send an email to the nominee
?>