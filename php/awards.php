<?php

    header("Access-Control-Allow-Headers: Authorization, Content-Type");
    header("Access-Control-Allow-Origin: *");
    header('content-type: application/json; charset=utf-8');
    $awards_arr = [];
    $response = new StdClass();
    $response->result = "";
    $response->msg = "";
    include("bootstrap.php");

    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    else {

       $i = 0; 
        $query = $mysqli->query("SELECT a.*, b.*, v.* FROM  Awards_Table a, User_Table b, Value_Table v WHERE a.uID=b.uID  AND a.vID=v.vID ORDER BY a.AwardDate desc");


                if($query){
// // Cycle through results
                    while ($row2 = $query->fetch_array()){  
                        $awards_arr[$i]['id'] = $row2["aID"];
                        $awards_arr[$i]['value'] = $row2["AwardName"];
    // Format the date
                        $date =  $row2["AwardDate"];
                        $awards_arr[$i]['date'] = date("d/m/Y", strtotime($date));
                        $awards_arr[$i]['name'] = $row2["Name"];
                        $awards_arr[$i]['jobtitle'] = $row2["JobTitle"];
                        $awards_arr[$i]['location'] = $row2["Location"];
                        $awards_arr[$i]['description'] = $row2["Description"];
// If a hero image has been uploaded pass the URL, otherwise pass the user image URL            
                        if ($row2["HeroImageURL"]!="") {
                            $awards_arr[$i]['ImageURL'] = $row2["HeroImageURL"];
                        }
                        elseif ($row2["ImageURL"]!="") {
                            $awards_arr[$i]['ImageURL'] = "uploads/".$row2["ImageURL"];
                        }
                        else {
                            $awards_arr[$i]['ImageURL'] = "dist/assets/img/avatar.jpg";
                        }
                        $i = $i + 1;
                    }
                    $query->close();
                } else {
                    echo($query);
                }
        
        $mysqli->close();
        echo json_encode($awards_arr);
    }
?>