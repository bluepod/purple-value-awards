//JQuery Module Pattern

// An object literal
var app = {
  init: function() {
    app.functionOne();
  },
  functionOne: function () {

  }
};


$('#exampleModal').on('hidden.bs.modal', function (e) {
  // do something...
  console.log("closing???");

$("#Location").val();

})

$("document").ready(function () {
  app.init();
  // set up datepicker for browsers that don't support it
//  if ( $('[type="date"]').prop('type') != 'date' ) {
//      $('[type="date"]').datepicker();
//  }
    if ($("#Awarddate").length) {
       $("#Awarddate").datepicker({ dateFormat: 'yy-mm-dd' });
    }
    
});

// extract data for nominations form
$(document).on("click", ".card.transform-on-hover", function() {
  // get the users name
  $("#modalForm").fadeIn();
  var userName = $(this).children(".card-body").children(".userName").text(),
      userEmail = $(this).children(".card-body").children("div[hidden]").children("p.userEmail").text(),
      userId = $(this).children(".card-body").children("div[hidden]").children("p.userId").text(),
      userJobTitle = $(this).children(".card-body").children(".userJob").text(),
      userLocation = $(this).children(".card-body").children(".userLocation").text();
 // console.log(userEmail);
  //console.log(userJobTitle);
  // assign it to the modal title
  $(".modal-title#nominationsTitle").html("NOMINATE " + userName);

//  console.log($('.modal-footer input[name="Id"]'));
  var hiddenName = $('.modal-footer input[name="Name"]'),
      hiddenEmail = $('.modal-footer input[name="Email"]'),
      hiddenId = $('.modal-footer input[name="Id"]'),
      hiddenJob = $('.modal-footer input[name="Jobtitle"]'),
      hiddenLocation = $('.modal-footer input[name="Location"]');
  // Set the hidden input fields for the form
  //console.log($('input[name="Email"]'));

//  console.log(  hiddenId.val()); 
  hiddenName.val(userName);
  hiddenId.val(userId);
  hiddenEmail.val(userEmail);
  hiddenJob.val(userJobTitle);
  hiddenLocation.val(userLocation);
//  console.log(hiddenEmail.val());
//  console.log(hiddenLocation.val());
 // console.log(hiddenJob.val());
  //hiddenEmail.val(userEmail);
  //hiddenId.val(userId);
});

// search function to show and hide divs containing nominations
$("#searchBox, #mainSearchBox").on("keyup", function() {
  // get search as a string
  var value = $(this).val().toLowerCase();
  $("#users .row > *").filter(function() {
    // check if empty and display all divs if so
    if (value == "") {
      $("#users .row > *").show();
    } else {
      // if not loop through each char and check it matches the names
      for (var i = 0; i < value.length; i++) {
        // get each char per iteration
        var currentSearchChar = $(".userName", this).text().toLowerCase().charAt(i);
        // check if it matches the search and display if yes
        if (currentSearchChar == value.charAt(i)) {
          $(this).show();
        } else {
          // if not hide the div and break the loop
          $(this).hide();
          break;
        }
      }
    }
  });
  $("#allawards .row > *").filter(function() {
    // check if empty and display all divs if so
    if (value == "") {
      $("#users .row > *").show();
    } else {
      // if not loop through each char and check it matches the names
      for (var i = 0; i < value.length; i++) {
        // get each char per iteration
        var currentSearchChar = $(".userName", this).text().toLowerCase().charAt(i);
        // check if it matches the search and display if yes
        if (currentSearchChar == value.charAt(i)) {
          $(this).show();
        } else {
          // if not hide the div and break the loop
          $(this).hide();
          break;
        }
      }
    }
  });  
});
// Stop form submission on hitting enter
$(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
});

// bring cursor to start of email form
$("input[type='email']").on("focus", function() {
  $(this).attr("type", "text");
  $(this)[0].setSelectionRange(0, 0);
  $(this).attr("type", "email");
});

// --------- admin page ---------- //

// click on edit user button
// function assigns user details to the form
$(document).on("click", ".card-body .editUserButton", function() {
  // hide add and award user forms
 //$("#addUserForm, #awardUserForm").hide();
  // show user form  

  $("#editUserForm").show();
  // assigning data from HTML
  var name = $(this).parent().parent().children().children("h6.userName").text(),
      email = $(this).parent().children("div[hidden]").children(".userEmail").text(),
      title = $(this).parent().parent().children().children(".userTitle").text(),
      location = $(this).parent().parent().children().children(".userLocation").text(),
      active = $(this).parent().children("div[hidden]").children(".userIsActive").text(),
      id = $(this).parent().children("div[hidden]").children(".userId").text()
      //modalTitle = "Edit " + name;

      var img = $(this).parent().parent().parent().children(".card").children(".card-img-top").attr('src');
      window.location.replace("add.html?n="+ name +"&id=" + id +"&e=" + email +"&t=" + title+ "&l=" + location + "&i=" + img);  
 // }
});


$(document).on("click", ".card-body .editAwardButton", function() {

var id = $(this).parent().children("div[hidden]").children(".AwardID").text();



window.location.replace("nominate.html?i=" + id);  

});


// click add new user button
$(".addNewUser").on("click", function() {
  window.location.replace("add.html");  
});

$(".addNewAward").on("click", function(e) {
window.location.replace("nominate.html");  

});

