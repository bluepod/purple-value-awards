// on resize
$( window ).resize(function() {
  $('.eHeight').height("auto");
})

var curAwards = [];
$(document).ready(function(){
// Add a new user   
  $("#addForm").on('submit', '#addUserForm', addUser);
  $("#nominationsForm").on('submit', nominateUser);
  $("#addForm").on('submit', '#editUserForm', updateUser);
  $("#awardForm").on('submit', '#awardUserForm', awardUser);
  $("#awardForm").on('submit', '#editAwardForm', updateAward);

//
//$("#users").on('click', '.awardUserButton', addAward);

});



function addUser(e) {
    
  e.preventDefault();

 //console.log("gere?")
  var formData = new FormData(this);
  //  for (var pair of formData.entries()) {
   //    console.log(pair[0] + ", " + pair[1]);
   //  }

  $.ajax({
    url: "https://value-awards.purple-agency.net/php/adduser.php",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend : function() {
        // Reset any messages
        $('#msg, #errmsg').html('').hide();   
    },
    success: function(data) {
      console.log(data);
      var response = data;

      console.log("success");

      if (response.result == "success") { 
        $("#addUserForm")[0].reset();
        $("#addForm").hide();
        $("#imageResize").hide();
        $("#addSuccess").show();

        //$('.modal').modal('toggle');
       // $('#msg').html(response.msg).fadeIn(500); 
          // Reload the hmtl page to include the new user    
        allUsers();
      }
      $('#errmsg').html(response.msg).fadeIn(500); 
    },
    // Display an error      
    error: function(jqXHR, textStatus, errorThrown) {
      $('#errmsg').html(jqXHR.responseText).fadeIn(500);
    }          
  });
  formData  = new FormData();
}

function updateUser(e) {
  e.preventDefault();


 // console.log("update");
  var formData = new FormData(this);

   // for (var pair of formData.entries()) {
   //    console.log(pair[0] + ", " + pair[1]);
   //  }



  $.ajax({
    url: "https://value-awards.purple-agency.net/php/updateuser.php",
    type: "POST",
    data:  formData,
    contentType: false,
         cache: false,
    processData:false,
    beforeSend : function()
    {
// Reset any messages
    $('#errmsg, #msg').html('').hide();   
    },
    success: function(data) {
      var response = data;

      //console.log ("return");

      if (response.result == "success") {

        $("#editUserForm")[0].reset();
        $("#addForm").hide();
        $("#imageResize").hide();
        $("#editSuccess").show();
      }
    },
    // Display an error      
    error: function(jqXHR, textStatus, errorThrown) {
      $('#errmsg').html(jqXHR.responseText).fadeIn(500);
    }          
  });
  formData  = new FormData();
}



// Make an award for the user
function awardUser(e) {
  e.preventDefault();

  //console.log("award");

   formData = new FormData(this);
  formData.append("Description", $('#Description div.ql-editor').html()); 

    for (var pair of formData.entries()) {
        console.log(pair[0] + ", " + pair[1]);
      }

  $('#msg, #errmsg').html('').hide();   

  
// start by checking that at least one checkbox has been selected
  $.ajax({
    url: "https://value-awards.purple-agency.net/php/awarduser.php",
    type: "POST",
    data:  formData,
    contentType: false,
         cache: false,
    processData:false,
    success: function(data) {
      var response = data;

      //console.log(response);

      if (response.result == "success") {
        $("#awardUserForm")[0].reset();

        $("#awardForm").hide();
        $("#awardSuccess").show();
      }
      else {
        $('#errmsg').html(response.msg).fadeIn(500); 
      }
    },
    // Display an error      
    error: function(jqXHR, textStatus, errorThrown) {
      $('#errmsg').html(jqXHR.responseText).fadeIn(500);
    }          
  });

  
}


// Update an award - usually to include the hero image
  function updateAward(e) {
  e.preventDefault();

  //  console.log('update');

   formData = new FormData(this);
   formData.append("Description", $('#Description div.ql-editor').html()); 
   for (var pair of formData.entries()) {
       console.log(pair[0] + ", " + pair[1]);
     }
 
    $('#msg, #errmsg').html('').hide();   
  // start by checking that at least one checkbox has been selected
  $.ajax({
    url: "https://value-awards.purple-agency.net/php/updateaward.php",
    type: "POST",
    data:   formData,
    contentType: false,
         cache: false,
    processData:false,
    success: function(data) {
console.log(data);

      var response = data;
      if (response.result == "success") {
        $("#editAwardForm")[0].reset();

        $("#awardForm").hide();
        $("#awardEditSuccess").show();
      }
      else {
        $('#errmsg').html(response.msg).fadeIn(500);
      }
    },
    // Display an error      
    error: function(jqXHR, textStatus, errorThrown) {
      $('#errmsg').html(jqXHR.responseText).fadeIn(500);
    }          
  });
}
// If a nomination was made 
  function nominateUser(e) {
  e.preventDefault();


//console.log("update");
  var formData = new FormData(this);


  $('#msg, #errmsg').html('').hide();   
// start by checking that at least one checkbox has been selected
  if ($(".form-check-input:checkbox:checked").checked){
    $('#errmsg').html("please select at least one value you are nominating for").fadeIn(500);
  }
  else {
    $.ajax({
      url: "https://value-awards.purple-agency.net/php/nominations.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
           cache: false,
      processData:false,
      success: function(data) {
        var response = data;
        if (response.result == "success") {
          $("#nominationsForm")[0].reset();
          $('.modal').modal('toggle');
          $('.toast-body').html(response.msg);
          $("#nominationsToast").toast('show');
        }
        else {
          $('#errmsg').html(response.msg).fadeIn(500); 
        }
      },
      // Display an error      
      error: function(jqXHR, textStatus, errorThrown) {
        $('#errmsg').html(jqXHR.responseText).fadeIn(500);
      }          
    });
      }
}
// Get all the users
function allUsers() {
    $.ajax({
      url: "https://value-awards.purple-agency.net/php/allusers.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#msg').html('').hide();   
      },
      success: function (response) {
          //console.log(response)
// Add an entry for each user in the gallery
        $('#users .fluid-container .row:last').html('');
        response.forEach(function(user){
            $('#users .fluid-container .row:last').append('<div class="col-md-6 col-lg-4 col-xl-2" style="margin-bottom: 30px;"><div class="card eHeight transform-on-hover"><img src="'+user.ImageURL+'" alt="'+user.name+'" class="card-img-top"><div class="card-body"><h6 class="userName">'+user.name+'</h6><p class="text-muted-new card-text mb-1 userTitle">'+user.jobtitle+'</p><p class="text-muted card-text userLocation">'+user.location+'</p><div class="clear"></div></div><div class="card-body"><button type="button" class="btn btn-purple editUserButton">Edit</button><div class="clear"></div><div hidden><p class="userEmail">'+user.email+'</p><p class="userId">'+user.id+'</p><p class="userIsActive">'+user.isactive+'</p></div></div></div></div>');
        });
          setTimeout(function(){
                // doing async stuff
                //console.log('task 1 in function1 is done!');
                //equalHeights()

            }, 1000);
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#msg').html(jqXHR.responseText).fadeIn(500);
      }
  });
}
// Get the active users
function activeUsers() {

$('#viewAllAwards').on("click",function (ev) {

 window.location.replace("awards.html"); 
});


    $.ajax({
      url: "https://value-awards.purple-agency.net/php/activeusers.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#errmsg, #msg').html('').hide();   
        //console.log("hello");
      },
      success: function (response) {
        //console.log(response);
// Add an entry for each user in the gallery
        $('#users .fluid-container .row:last').html('');
        response.forEach(function(user){
            var divCont = '<div class="col-md-6 col-lg-4 col-xl-2" style="margin-bottom: 30px;"><div class="card fullscreen eHeight transform-on-hover" data-toggle="modal" data-target="#exampleModal"><img src="dist/assets/img/star.png" alt="Star" class="star" data-toggle="modal" data-target="#exampleModal"><img src='+user.ImageURL+' alt="'+user.name+'" class="card-img-top"><div class="card-body"><p class="userName">'+user.name+'</p><p class="userJob text-muted-new">'+user.jobtitle+'</p><p class="userLocation text-muted-new">'+user.location+'</p><div hidden><p class="userEmail">'+user.email+'</p><p class="userId">'+user.id+'</p><p class="userIsActive">'+user.isactive+'</p></div></div></div>';        
             $('#users .fluid-container .row:last').append(divCont);
        });
        setTimeout(function(){
        // doing async stuff
        //console.log('task 1 in function1 is done!');
        //equalHeights()

    }, 1000);


      },
      error: function(jqXHR, textStatus, errorThrown) {
          console.log("error");
         $('#msg').html(jqXHR.responseText).fadeIn(500);
      }
    });
}


function equalHeights(){

  console.log("equal");

   $('.awards').each(function(){  
   // Cache the highest
      var highestBox = 0;
    // Select and loop the elements you want to equalise
      $('.eHeight', this).each(function(){        
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }      
      });        

      //console.log(highestBox); 

      $('.eHeight',this).height(highestBox);

    });
    
    $("#users .row").each(function(){
          // Cache the highest
          var highestBox = 0;
        // Select and loop the elements you want to equalise
          $('.eHeight', this).each(function(){        
            if($(this).height() > highestBox) {
              highestBox = $(this).height(); 
            }      
          });        

          //console.log(highestBox); 

          $('.eHeight',this).height(highestBox);                
    });





        $(".currenta").each(function(){
          // Cache the highest
          var highestBox = 0;
        // Select and loop the elements you want to equalise
          $('.eHeight', this).each(function(){        
            if($(this).height() > highestBox) {
              highestBox = $(this).height(); 
            }      
          });        

         console.log("currnet" + highestBox); 

          $('.eHeight',this).height(highestBox);                
    });




}


// Get all the values - not used at this time!!
function value(id, quill) {
    $.ajax({
      url: "https://value-awards.purple-agency.net/php/singleaward.php",
      type: "post",
      dataType: 'json',
      data:  {id: id},
      beforeSend : function()
      {
// Reset any messages
        $('#msg').html('').hide(); 
      },
      success: function (response) {
         $('.awardTitle').text("Editing value award for "+response[0].name);       
         $("#awardButton").val("Edit user");
         $("#awardUserForm").attr('id','editAwardForm');


        $('#Award').val(response[0].valueID);
          var aDate= response[0].date.split("/");
        $('#Awarddate').val(aDate[2] + "-" + aDate[1] + "-"+aDate[0]);

       //  $('#Description').html(response[0].description);
       quill.clipboard.dangerouslyPasteHTML(response[0].description);

        $('#awardImage').attr('src','https://value-awards.purple-agency.net/uploads/'+response[0].ImageURL);
         
        $("#Awardname").val(response[0].name);

        $('#Id').val(response[0].id);
  
    
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#msg').html(jqXHR.responseText).fadeIn(500);
      }
  });
}


// Get all the values - not used at this time!!
function values() {
    $.ajax({
      url: "https://value-awards.purple-agency.net/php/allawards.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#msg').html('').hide(); 
      },
      success: function (response) {

 //        console.log(response);
// Add an entry for each user in the gallery
        $('#allawards .fluid-container .row:last').html('');
        response.forEach(function(award){
// If this is an admin page ensure an update button is included
        var divCont = '<div class="col-md-6 col-lg-4 col-xl-2" style="margin-bottom: 30px;"><div class="card"><img src='+award.ImageURL+' alt="'+award.name+'" class="card-img-top"><div class="card-body"><p class="award">'+award.value+'<br>'+award.date+'</p><p class="userName">'+award.name+'</p><p class="userJob">'+award.jobtitle+'</p><p class="userLocation">'+award.location+'</p><button type="button" class="btn btn-purple editAwardButton">Edit</button><div class="clear"></div><div hidden><p class="AwardID">'+award.id+'</p></div></div></div>';
        $('#allawards .fluid-container .row:last').append(divCont);
       });    
    
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#msg').html(jqXHR.responseText).fadeIn(500);
      }
  });
}



// Get the award winners who are not current holders
function awards(admin) {
    $.ajax({
      url: "https://value-awards.purple-agency.net/php/awards.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#msg').html('').hide();  
      },
      success: function (response) {
//        console.log(response);
// Add an entry for each user in the gallery
        $('#awards .fluid-container .row:last').html('');
        response.forEach(function(award){
// If this is an admin page ensure an update button is included
        var divCont = '<div class="col-md-6 col-lg-4 col-xl-2" style="margin-bottom: 30px;"><div class="card fullscreen eHeight transform-on-hover"><img src="https://value-awards.purple-agency.net/'+award.ImageURL+'"" alt="'+award.name+'" class="card-img-top"><div class="card-body"><p class="award">'+award.value+'<br>'+award.date+'</p><p class="userName">'+award.name+'</p><p class="userJob">'+award.jobtitle+'</p><p class="userLocation">'+award.location+'</p><div class="showOnHover"><p class="text-white card-text">'+award.description+'</p></div></div></div></div>';
        $('#awards .fluid-container .row:last').append(divCont);
       });       

        // fullscreen();
        //equalHeights();
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#msg').html(jqXHR.responseText).fadeIn(500);
      }
  });
}
// Get the current award holders
function currentHolders(admin, current, callback) {
    $.ajax({
      url: "https://value-awards.purple-agency.net/php/current.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#msg, #errmsg').html('').hide();  
      },
      success: function (response) {
          console.log(response)
// Add an entry for each user in the gallery
        $('#current .fluid-container .row:last').html('');
        response.forEach(function(award){             
          var divCont = '<div class="col-md-6 col-lg-4 col-xl-2" style="margin-bottom: 30px;"><div class="card fullscreen eHeight transform-on-hover"><img src="https://value-awards.purple-agency.net/'+award.ImageURL+'"" alt="'+award.name+'" class="card-img-top"><div class="card-body"><p class="award">'+award.value+'<br>'+award.date+'</p><p class="userName">'+award.name+'</p><p class="userJob">'+award.jobtitle+'</p><p class="userLocation">'+award.location+'</p><div class="showOnHover"><p class="text-white card-text">'+award.description+'</p></div></div></div></div>';
          $('#current .fluid-container .row:last').append(divCont);
        });       

        console.log("added the current winners")

        //equalHeights();
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#errmsg').html(jqXHR.responseText).fadeIn(500);
      }
  });
   if(callback) callback();  
}

// Get the current award holders
function currentAwards() {

console.log('awards v2');

    $.ajax({
      url: "https://value-awards.purple-agency.net/php/current-v2.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#msg, #errmsg').html('').hide();  
      },
      success: function (response) {
          
        var i = 1;
        while ( i < 7) {
          $("#award-" + i + " .card").append('<div class="card-header" style="background-color: #b43d67; color: #fff265;">Current Winner</div>');
          $("#award-" + i + " .card").append('<div class="carousel slide carousel-fade" data-ride="carousel"><div class="carousel-inner"></div></div>');  
          i++;

        }
        
        $.each(response, function(key, value) {
           value.forEach(function(single){
               var awardID = single.vid;
               var name = single.name;
               var imageURL = single.ImageURL;
               var description = single.description;
               $("#award-" + awardID).addClass("award-box")
               $("#award-" + awardID + " .carousel .carousel-inner").append('<div class="carousel-item">' +
                                                                                '<img src="http://value-awards.purple-agency.net/' + imageURL + '" alt="' + name + '" class="card-img-top">' +
                                                                                '<div class="card-footer" style="background-color: #589188; color: #f7f7f7;">' + name + '</div>' +
                                                                            '</div>');
           });
           $(".award-box .carousel-inner .carousel-item:first-of-type").addClass("active");
        });


// Add an entry for each user in the gallery
  //      $('#current .fluid-container .row:last').html('');
  //      response.forEach(function(award){             
  //        var divCont = '<div class="col-md-6 col-lg-4 col-xl-2" style="margin-bottom: 30px;"><div class="card fullscreen eHeight transform-on-hover"><img src="https://value-awards.purple-agency.net/'+award.ImageURL+'"" alt="'+award.name+'" class="card-img-top"><div class="card-body"><p class="award">'+award.value+'<br>'+award.date+'</p><p class="userName">'+award.name+'</p><p class="userJob">'+award.jobtitle+'</p><p class="userLocation">'+award.location+'</p><div class="showOnHover"><p class="text-white card-text">'+award.description+'</p></div></div></div></div>';
  //        $('#current .fluid-container .row:last').append(divCont);
  //      });       

  //      console.log("added the current winners")

        //equalHeights();
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#errmsg').html(jqXHR.responseText).fadeIn(500);
      }
  });
}

function getAwardUser(){

  var quill = new Quill('#Description', { theme: 'snow'});

  var urlParams = new URLSearchParams(window.location.search);

  var id = urlParams.get('i');

  if (id){
    
    value(id, quill);
  } else {


  $.ajax({
      url: "https://value-awards.purple-agency.net/php/users.php",
      type: "post",
      dataType: 'json',
      beforeSend : function()
      {
// Reset any messages
        $('#msg').html('').hide();   
      },
      success: function (response) {
        //  console.log(response);

           $('#Awardname').autocomplete({
                minLength: 0,
                source: response ,
                select: function (event, ui) {

                    $('#Awardname').val(ui.item.label); // display the selected text
                    $('#user-id').val(ui.item.value);

                    $('#Id').val(ui.item.value);
                     return false;
                 },
                change: function( event, ui ) {
                    //$('#searchPeople').val( ui.item? ui.item.label : 0 );
                    
                } 
            });
      },
      error: function(jqXHR, textStatus, errorThrown) {
         $('#msg').html(jqXHR.responseText).fadeIn(500);
      }
  });

  }

}


function getAddUser(){

  var urlParams = new URLSearchParams(window.location.search);

if (urlParams.get('n')){

  var name = urlParams.get('n');
  var id = urlParams.get('id');
  var title = urlParams.get('t');
  var location = urlParams.get('l');
  var img = urlParams.get('i');

  var filename = img.split("/")

  var email = urlParams.get('e');


      $("form").attr("id","editUserForm");
      $(".editUser").text("Edit User " + name);

      $("#Name").val(name);
      $("#Jobtitle").val(title);
      $("#Location").val(location);
      $("#Email").val(email);
        $("#Email").attr('readonly',true);
      $("#userImage").val(filename[1]);
      
      $("#awardImage").attr('src','https://value-awards.purple-agency.net/' + urlParams.get('i'));
      $("#awardButton").val("Edit user");

    $(".isActive").show();


  $('input[name="Id"]').attr("value",id);

  }

}

  function demoUpload() {
    var $uploadCrop;

    function readFile(input) {
      if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
          $('.upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                  url: e.target.result
                }).then(function(){
                  console.log('jQuery bind complete');
                });
                
              }
              
              reader.readAsDataURL(input.files[0]);
          }
          else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
      viewport: {
        width: 400,
        height: 400,
        type: 'square'
      },
      enableExif: true
    });

    $('#upload').on('change', function () { readFile(this); });
    $('.upload-result').on('click', function (ev) {
      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function (resp) {
        
        $('#awardImage').attr('src',resp);
        $('#imagebase64').val(resp);

      });
    });
  }


/*
  function fullscreen(){



    var full = $(".fullscreen");
  //Loops over all elements that have the class fullscreen
  full.each(function(index, elem) {
    $(elem).hover(fullscreenClick);
  });

  function fullscreenClick() {
    //The button is this
    //We want to clone the parent
    var box = $(this).parent();
    //create a holder box so the layout stays the same
    var holder = $(box).clone(false, true);
    //and make it not visible
    $(holder).css({
      "visibility": "hidden"
    });

    //Get its position
    var pos = $(box).position();

    //Substitute our box with our holder
    $(box).before($(holder));

    //Set the position of our box (not holder)
    //Give it absolute position (eg. outside our set structure)
    $(box).css({
      "position": "absolute",
      "left": pos.left + "px",
      "top": pos.top + "px",
    });

    //Set class so it can be animated
    $(box).addClass("fullscreen");

    //Animate the position
    $(box).animate({
      "top": 200,
      "left": 100,
    }, 1500);

  }
  
  }*/
